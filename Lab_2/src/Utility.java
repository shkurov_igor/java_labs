import Exceptions.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Utility {
    public static int scanForInt(Scanner scan) throws IOException {
        int size = 0;
        if (scan.hasNextInt())
        {
            size = scan.nextInt();
            if (size > 1000000) {
                throw new TooManyObjectsException("Matrix size is overkill.\n");
            }
        }
        else
        {
            throw new IncorrectDataFormat("Matrix size has to be integer value.\n");
        }

        return size;
    }
    public static int readSize(String filename) throws IOException {
        try (FileReader fr = new FileReader(filename)) {
            Scanner scan = new Scanner(fr);

            return scanForInt(scan);
        }
    }

    public static void matrixOperation(Matrix matrix) throws ZeroDivisionException {
        matrix.fillMatrix();
        matrix.printMatrix();
        for (int i = 0; i < 3; i++)
        {
            matrix.anticlockwiseTurn();
        }
        matrix.division();
    }
}
