package Exceptions;

public class ZeroDivisionException extends ArithmeticException {

    public ZeroDivisionException(String info) {
        super(info);
    }
}
