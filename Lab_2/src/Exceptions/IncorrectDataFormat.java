package Exceptions;

import java.io.IOException;

public class IncorrectDataFormat extends IOException {

    public IncorrectDataFormat(String info) {
        super(info);
    }
}
