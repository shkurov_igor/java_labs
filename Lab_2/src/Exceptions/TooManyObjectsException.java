package Exceptions;

import java.io.IOException;

public class TooManyObjectsException extends IOException {

    public TooManyObjectsException(String info) {
        super(info);
    }
}
