import java.util.Random;

import Exceptions.ZeroDivisionException;

public class Matrix {
    private double[][] matrix;
    private final int size;

    Matrix(int size) {
        matrix = new double[size][size];
        this.size = size;
    }

    public void fillMatrix() {
        Random rnd = new Random();
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                matrix[i][j] = (rnd.nextDouble() * this.size * 2) - this.size;
            }
        }
    }

    public void printMatrix() {
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                System.out.print(Math.round(matrix[i][j] * 100.0) / 100.0 + " ");
            }
            System.out.print('\n');
        }
        System.out.print('\n');
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                str.append(Math.round(matrix[i][j] * 100.0) / 100.0);
                str.append(' ');
            }
            str.append('\n');
        }
        str.append('\n');
        return str.toString();
    }

    public void anticlockwiseTurn() {
        double[][] turnedMatrix = new double[this.size][this.size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                turnedMatrix[i][j] = matrix[j][size - 1 - i];
            }
        }
        matrix = turnedMatrix;
    }

    public void division() throws ZeroDivisionException {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                double sum = 0;
                if (i != 0) {
                    sum += matrix[i - 1][j];
                }
                if (i != size - 1) {
                    sum += matrix[i + 1][j];
                }
                if (j != 0) {
                    sum += matrix[i][j - 1];
                }
                if (j != size - 1) {
                    sum += matrix[i][j + 1];
                }
                if (sum == 0)
                {
                    throw new ZeroDivisionException("Attempt of dividing by zero.\n");
                }
                matrix[i][j] /= sum;
            }
        }
    }

}
