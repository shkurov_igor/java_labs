import Exceptions.ZeroDivisionException;

import java.io.*;


public class Main {

    public static void main(String[] args) {
        int size = 0;

        try {
            size = Utility.readSize("matrix.txt");
        }
        catch (IOException exception) {
            System.out.println(exception.getMessage());;
        }

        Matrix[] matrixArr = {new Matrix(size), new Matrix(size), new Matrix(size)};

        try (FileWriter fw = new FileWriter("matrix_out.txt")) {
            for (Matrix matrix : matrixArr) {
                Utility.matrixOperation(matrix);
                fw.write(matrix.toString());
            }
        }
        catch (IOException exception) {
            System.out.println("File is not accessible.\n");
        }
        catch (ZeroDivisionException exception) {
            exception.printStackTrace();
        }
    }
}
