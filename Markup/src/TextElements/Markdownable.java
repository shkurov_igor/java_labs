package TextElements;

public interface Markdownable {
    void toMarkdown(StringBuilder sb);
}
