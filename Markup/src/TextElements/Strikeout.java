package TextElements;

import java.util.List;

public class Strikeout implements Markdownable {
    private final StringBuilder str;

    @Override
    public String toString() {
        return str.toString();
    }

    public Strikeout(List<Markdownable> list) {
        str = new StringBuilder("~");
        list.forEach(str::append);
        str.append("~");
    }

    public void toMarkdown(StringBuilder sb) {
        sb.append(str);
    }
}
