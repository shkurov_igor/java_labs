package TextElements;

public class Text implements Markdownable {
    private final String str;

    @Override
    public String toString() {
        return str;
    }

    public Text(String str) {
        this.str = str;
    }

    public void toMarkdown(StringBuilder sb) {
        sb.append(str);
    }
}
