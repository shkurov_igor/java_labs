package TextElements;

import java.util.List;

public class Paragraph implements Markdownable {
    private final StringBuilder str;

    @Override
    public String toString() {
        return str.toString();
    }

    public Paragraph(List<Markdownable> list) {
        str = new StringBuilder();
        list.forEach(str::append);
    }

    public void toMarkdown(StringBuilder sb) {
        sb.append(str);
    }
}
