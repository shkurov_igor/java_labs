package TextElements;

import java.util.List;

public class Strong implements Markdownable {
    private final StringBuilder str;

    @Override
    public String toString() {
        return str.toString();
    }

    public Strong(List<Markdownable> list) {
        str = new StringBuilder("__");
        list.forEach(str::append);
        str.append("__");
    }

    public void toMarkdown(StringBuilder sb) {
        sb.append(str);
    }
};
