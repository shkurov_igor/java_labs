import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class StreamFunctions {
    public static float averageInList(List<Integer> list) {
        return list.stream()
                .reduce(
                        0, Integer::sum) / (float) list.size();
    }

    public static List<String> upperCaseAndNew(List<String> list) {
        return list.stream()
                .map((str) -> "_new_" + str.toUpperCase())
                .collect(
                        Collectors.toList());
    }

    public static List<Integer> onlySquares(List<Integer> list) {
        return list.stream()
                .distinct()
                .map((x) -> x * x)
                .collect(Collectors.toList());
    }

    public static List<String> findByLetterAndSort(List<String> list, char a) {
        return list.stream()
                .filter((str) -> str.charAt(0) == a)
                .sorted()
                .collect(
                        Collectors.toList());
    }

    public static Integer getLastElement(List<Integer> list) throws NoSuchElementException {
        if (list.size() == 0) {
            throw new NoSuchElementException();
        }

        return list.stream()
                .skip(list.size() - 1)
                .findAny()
                .orElseThrow(NoSuchElementException::new);
    }

    public static Integer sumOfEvenNumbers(List<Integer> list) {
        return list.stream()
                .filter((x) -> x % 2 == 0)
                .reduce(0,
                        Integer::sum);
    }

    public static Map<Character, String> listToMap(List<String> list) {
        return list.stream()
                .collect(
                        Collectors.toMap(
                                (str) -> str.charAt(0),
                                (str) -> str.substring(1)));
    }
}
