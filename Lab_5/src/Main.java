import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> nums = List.of(1, 2, 2, 3, 3, 3, 4, 4, 5, 5, 6); //1
        System.out.println(StreamFunctions.averageInList(nums));
        System.out.println("-----");

        List<String> strings = List.of("abc", "lol", "lab");
        StreamFunctions.upperCaseAndNew(strings).forEach(System.out::println); //2
        System.out.println("-----");

        StreamFunctions.onlySquares(nums).forEach(System.out::println); //3
        System.out.println("-----");

        StreamFunctions.findByLetterAndSort(strings, 'l').forEach(System.out::println); //4
        System.out.println("-----");

        try { //5
            System.out.println(StreamFunctions.getLastElement(nums));
            System.out.println(StreamFunctions.getLastElement(List.of()));
        }
        catch (NoSuchElementException e) {
            System.out.println("No last element found ;(");
        }
        System.out.println("-----");

        System.out.println(StreamFunctions.sumOfEvenNumbers(nums)); //6
        System.out.println("-----");

        List<String> stringsForMap = List.of("a_flow", "p_kick", "o_loss"); //7
        StreamFunctions.listToMap(stringsForMap).forEach((p, v) -> System.out.println("key: " + p + " value: " + v));
    }
}
