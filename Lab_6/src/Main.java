public class Main {
    public static void main(String[] args) {
        Thread supervisor = new Thread(new SupervisorTask());
        supervisor.setName("supervisor");
        supervisor.start();
    }
}
