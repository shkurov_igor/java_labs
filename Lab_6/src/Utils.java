import javax.swing.plaf.TableHeaderUI;

public class Utils {
    static public void pause(long millis) {
        try {
            Thread.sleep(millis);
        }
        catch(InterruptedException e) {
            System.out.println("lol");
        }
    }

    static public void runningDots() {
        for (int i = 3; i > 0; i--) {
            for (int j = i; j > 0; j--) {
                System.out.print('.');
            }
            pause(1000);
            for (int j = i; j > 0; j--) {
                System.out.print('\b');
            }
        }
    }
}
