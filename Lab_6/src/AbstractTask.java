import java.util.Random;

public class AbstractTask implements Runnable {
    public final Object mutex = new Object();
    public enum progState {
        UNKNOWN,
        STOPPING,
        RUNNING,
        FATAL_ERROR,
    }

    private progState state;

    private class DaemonTask implements Runnable {
        Random rnd = new Random();
        @Override
        public void run() {
            while (true) {
                Utils.pause(4000);
                synchronized (mutex) {
                    int stateNum = rnd.nextInt(progState.values().length);
                    state = progState.values()[stateNum];
                    System.out.println("Daemon changed state to: " + state);
                    mutex.notify();
                }
            }
        }
    }

    @Override
    public void run() {
        System.out.println("Program has started.");
        state = progState.RUNNING;
        Thread daemon = new Thread(new DaemonTask());
        daemon.setDaemon(true);
        daemon.setName("daemon_thread");
        daemon.start();

        while (true) { //true
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                break;
            }
//            Utils.pause(4000);
            System.out.println("Program has successfully worked!");
        }
        System.out.println("Program was closed");
    }

    public progState getState() {
        return state;
    }

    @SupervisorAccessible
    private void setState(progState state) {
        this.state = state;
    }
}

