import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SupervisorTask extends Thread {
    private final AbstractTask program = new AbstractTask();

    @Override
    public void run() {
        Thread task = new Thread(program);
        task.setName("program");
        task.start();
        while (true) {
            synchronized (program.mutex) {
                try {
                    program.mutex.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (program.getState() == AbstractTask.progState.UNKNOWN ||
                        program.getState() == AbstractTask.progState.STOPPING) {
                    restartProgram();
                    System.out.println("Supervisor changed state to: RUNNING");
                }

                if (program.getState() == AbstractTask.progState.FATAL_ERROR) {
                    task.interrupt();
                    break;
                }
            }
        }
        System.out.println("Supervisor was closed");
    }

    private void restartProgram() {
        Class<?> cl = program.getClass();

        for (Method method : cl.getDeclaredMethods()) {
            if (method.isAnnotationPresent(SupervisorAccessible.class)) {
                method.setAccessible(true);
                try {
                    method.invoke(program, AbstractTask.progState.RUNNING);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
