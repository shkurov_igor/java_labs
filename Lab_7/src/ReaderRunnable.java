import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ReaderRunnable implements Runnable {
    private final BlockingQueue<String> queue;

    public ReaderRunnable(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        Utils.pause(2000 + (int) (Math.random() * 5000));
        try {
            String message = queue.take();
            System.out.println(Thread.currentThread().getName() + " has just read this message: \"" + message + "\"");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
