import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class Utils {
    static int readerN;
    static int writerN;

    public static void pause(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class ThreadPerTaskExecutor implements Executor {
        public void execute(Runnable r) {
            Thread thread = new Thread(r);
            if (r.getClass().isAssignableFrom(ReaderRunnable.class)) {
                thread.setName("Reader_" + readerN);
                ++readerN;
            }
            else if (r.getClass().isAssignableFrom(WriterRunnable.class)) {
                thread.setName("Writer_" + writerN);
                ++writerN;
            }
            else {
                throw new RuntimeException("Argument should be either ReaderRunnable or WriterRunnable");
            }

            thread.start();
        }
    }
}
