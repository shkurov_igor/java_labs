import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) {
        BlockingQueue<String> queue = new LinkedBlockingQueue<>();
        int n = Integer.parseInt(args[0]);
        System.out.println("Number of threads: " + n);

        Utils.ThreadPerTaskExecutor execWriter = new Utils.ThreadPerTaskExecutor();
        Utils.ThreadPerTaskExecutor execReader = new Utils.ThreadPerTaskExecutor();

        for (int i = 0; i < n; i++) {
            execReader.execute(new WriterRunnable(queue));
            execWriter.execute(new ReaderRunnable(queue));
        }
    }
}
