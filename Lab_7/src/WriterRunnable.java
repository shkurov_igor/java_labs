import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class WriterRunnable implements Runnable {
    private final BlockingQueue<String> queue;

    public WriterRunnable(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        String message;
        Utils.pause(2000 + (int) (Math.random() * 5000));
        try {
            message = Thread.currentThread().getName() + " wrote this message.";
            System.out.println(message);
            queue.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
