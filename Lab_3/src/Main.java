import AnimalQueue.AnimalQueue;

import Hierachy.*;

import Hierachy.ArthropodaClasses.*;

import Hierachy.ArthropodaClasses.ArachnidaOrders.*;
import Hierachy.ArthropodaClasses.BranchiopodaOrders.*;
import Hierachy.ArthropodaClasses.MalacostracaOrders.*;

import Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.*;
import Hierachy.ArthropodaClasses.BranchiopodaOrders.NotostracaFamilies.*;
import Hierachy.ArthropodaClasses.MalacostracaOrders.DecapodaFamilies.*;

import Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.CaddidaeGenera.*;
import Hierachy.ArthropodaClasses.BranchiopodaOrders.NotostracaFamilies.TriopsidaeGenera.*;
import Hierachy.ArthropodaClasses.MalacostracaOrders.DecapodaFamilies.SpongicolidaeGenera.*;

import Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.CaddidaeGenera.CaddoSpecies.*;
import Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.CaddidaeGenera.CadellaSpecies.*;
import Hierachy.ArthropodaClasses.BranchiopodaOrders.NotostracaFamilies.TriopsidaeGenera.TriopsSpecies.*;
import Hierachy.ArthropodaClasses.MalacostracaOrders.DecapodaFamilies.SpongicolidaeGenera.EngystenopusSpecies.*;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        AnimalQueue queue = new AnimalQueue();

        queue.add(TriopsCancriformis.class);
        queue.add(EngystenopusPalmipes.class);
        queue.add(CaddoAgilis.class);
        queue.add(TriopsAustraliensis.class);
        queue.add(CaddoPepperella.class);
        queue.add(CaddellaAfricana.class);
        queue.add(CaddellaCapensis.class);
        queue.printQueue();
        queue.produce(Caddo.class).printQueue();

        AnimalQueue queue2 = new AnimalQueue();

        queue2.add(Branchiopoda.class);
        queue2.add(Decapoda.class);
        queue2.add(Caddidae.class);
        queue2.add(Triopsidae.class);
        queue2.add(Engystenopus.class);
        queue2.add(Spongicolidae.class);


        AnimalQueue borderList = new AnimalQueue();
        borderList.add(EngystenopusPalmipes.class);
        borderList.add(Triops.class);
        LinkedList<AnimalQueue> list =  queue2.consume(borderList);
        list.get(0).printQueue();
        list.get(1).printQueue();


//        System.out.println(Engystenopus.class.isAssignableFrom(EngystenopusPalmipes.class));
    }
}
