package Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.CaddidaeGenera.CadellaSpecies;

import Hierachy.ArthropodaClasses.ArachnidaOrders.OpilionesFamilies.CaddidaeGenera.Caddella;

final public class CaddellaAfricana extends Caddella {
    @Override
    public String toString() { return "Африканский павук"; }
}
