package AnimalQueue;

import Hierachy.Arthropoda;

import java.util.LinkedList;

public class AnimalQueue {
    private final LinkedList<Class<? extends Arthropoda>> list;

    public AnimalQueue() {
        list = new LinkedList<>();
    }

    private AnimalQueue(LinkedList<Class<? extends Arthropoda>> list) {
        this.list = list;
    }

    public void add(Class<? extends Arthropoda> element) {
        list.add(element);
    }

    public Class<? extends Arthropoda> get(int index) {
        return list.get(index);
    }

    public int size() { return list.size(); }

    public void printQueue() {
        StringBuilder strB = new StringBuilder();
        for (Class<? extends Arthropoda> el: list) {
            strB.append("(*)");
            strB.append(el.getSimpleName());
            strB.append(' ');
        }
        System.out.println(strB);
    }

    public AnimalQueue produce(Class<? extends Arthropoda> upperBound) {
        LinkedList<Class<? extends Arthropoda>> newList = new LinkedList<>(list);
        newList.removeIf(el -> !upperBound.isAssignableFrom(el));
        return new AnimalQueue(newList);
    }

    public LinkedList<AnimalQueue> consume(AnimalQueue lowerBounds) {
        LinkedList<Class<? extends Arthropoda>> tempList = new LinkedList<>(list);
        int count = lowerBounds.size();
        LinkedList<AnimalQueue> retList = new LinkedList<>();

        for (int i = 0; i < count; i++) {
            retList.add(new AnimalQueue());
            for (Class<? extends Arthropoda> aClass : tempList) {
                if (aClass.isAssignableFrom(lowerBounds.get(i))) {
                    retList.get(i).add(aClass);
                }
            }
        }
        return retList;
    }
}
