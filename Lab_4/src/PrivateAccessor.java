import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.lang.reflect.Method;

public class PrivateAccessor {
    public void gainAccess() throws InvocationTargetException, IllegalAccessException {
        Class<?> cl = PrivateAccessMethods.class;

        for (Method method: cl.getDeclaredMethods()) {
            if (method.isAnnotationPresent(CustomAnnotation.class)) {
                method.setAccessible(true);
                for (int i = 0; i < method.getAnnotation(CustomAnnotation.class).reps(); i++) {
                    System.out.println(method.getName() + ':');
                    System.out.println(method.invoke(null, 10, 5));
                }
            }
        }
    }
}
