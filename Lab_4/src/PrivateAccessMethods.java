import java.util.Collection;
import java.util.List;

public class PrivateAccessMethods {
    @CustomAnnotation
    private static <T extends Comparable<T>> T absMax(T a, T b) {
        if (a == null) {
            if (b == null) {
                return a;
            }
            return b;
        }
        if (b == null) {
            return a;
        }
        return a.compareTo(b) > 0 ? a : b;
    }

    @CustomAnnotation(reps = 2)
    private static int sum(int a, int b) {
        return a + b;
    }

    @CustomAnnotation(reps = 3)
    private static int sub(int a, int b) {
        return a - b;
    }
}
