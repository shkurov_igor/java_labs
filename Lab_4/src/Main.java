import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args)  {
        PrivateAccessor pa = new PrivateAccessor();
        try {
            pa.gainAccess();
        } catch (InvocationTargetException | IllegalAccessException e){
            e.printStackTrace();
        }
    }
}
