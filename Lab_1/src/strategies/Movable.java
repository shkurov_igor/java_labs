package Strategies;

public interface Movable {
    public void move();
}
