package Strategies;

public class RidingHorse implements Movable {
    public void move() {
        System.out.println("I'm gonna take my horse...");
    }
}
