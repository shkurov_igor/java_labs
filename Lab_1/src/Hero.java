import Strategies.Movable;

public class Hero {
    private Point location;

    Hero(int x, int y) {
        this.location = new Point(x, y);
        System.out.println("Hero was spawned on " + this.location + '\n');
    }

    public void move(Movable strategy, Point point) {
        strategy.move();
        System.out.println("Hero's location changed from " + this.location + " to " + point + '\n');
        this.location = point;
    }

    public void getLocation() {
        System.out.println("Hero's current location is " + this.location);
    }

}
