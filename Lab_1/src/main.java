import Strategies.*;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random rnd = new Random();
        Hero hero = new Hero(1, 2);
        Movable[] actions = {new Walking(), new Flying(), new RidingHorse()};

        for (Movable a :
                actions) {
            hero.move(a, new Point(rnd.nextInt(101), rnd.nextInt(101)));
        }
        hero.getLocation();
    }
}
